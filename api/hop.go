package api

import (
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Config contains Hop scrape config values
type Config struct {
	URL         string
	CSSRegex    string
	LookupType  string
	FollowLinks bool
	OutputDir   string
	FollowDepth int
}

// Hop scrapes the passed url for items requested. options can be passed to filter out items from being scrapped.
func Hop(url string, options ...func(*Config)) (Config, error) {
	h := Config{
		CSSRegex:    ".*",
		LookupType:  "text",
		FollowLinks: false,
		OutputDir:   "./output",
		FollowDepth: 0,
	}

	if url == "" {
		return h, errors.New("url is required")
	}

	h.URL = url

	for _, option := range options {
		option(&h)
	}

	return h, nil
}

// Scrape finds items in a provided url that match the type requested
func (h *Config) Scrape() (int, error) {
	if h.OutputDir == "" {
		h.OutputDir = "./output"
	}

	resp, err := getURL(h.URL)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	var re = regexp.MustCompile(`(?mi)2[0-9]{1,2}`)
	if !re.MatchString(resp.Status) {
		return 0, errors.Errorf("Issue getting url \"%s\", got a return status of %s\n", h.URL, resp.Status)
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	htmlString := string(b)
	var count int

	switch strings.ToLower(h.LookupType) {
	case "images":
		count = downloadContent(`(?m)\<\s?img.*?srcS?e?t?\=[\"\'](.*?\.(jpe?g|png|gif))`, h.URL, htmlString, h.OutputDir)
		break
	case "links":
		count = downloadTextData(`(?mi)\<\s?a.*?href\=[\"\'](.*?)[\'\"]`, htmlString, h.OutputDir)
		break
	default:
		count = downloadTextData(`(?mi)\<\s?p\s?>([\s\S]*?)<\/\s?p\>|\<\s?h[1-6]\s?>([\s\S]*?)<\/\s?h[1-6]\>`, htmlString, h.OutputDir)
		break
	}

	return count, nil
}

func getURL(u string) (*http.Response, error) {
	parsedURL, err := url.Parse(u)
	if err != nil {
		return nil, err
	}

	if parsedURL.Hostname() == "" {
		println("no hostname")
	}

	if !parsedURL.IsAbs() {
		parsedURL.Scheme = "https"
	}

	resp, err := http.Get(parsedURL.String())
	if err != nil {
		return nil, errors.Errorf("could not get response from URL %s. %v\n", parsedURL.String(), err)
	}

	return resp, nil
}

func openFile(fName string) (*os.File, error) {
	var f *os.File

	_, err := os.Stat(fName)
	if os.IsNotExist(err) {
		_, err = os.Create(fName)
		if err != nil {
			return f, err
		}
	}

	f, err = os.OpenFile(fName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		return f, err
	}

	return f, err
}

func writeTextToFile(fName string, t string) error {
	f, err := openFile(fName)
	defer f.Close()
	if err != nil {
		return err
	}

	_, err = f.Write([]byte(fmt.Sprintf("%s\n", t)))
	if err != nil {
		return err
	}

	f.Sync()

	return nil
}

func writeContentToFile(fName string, c io.Reader) error {
	f, err := openFile(fName)
	defer f.Close()
	if err != nil {
		return err
	}

	_, err = io.Copy(f, c)
	if err != nil {
		return err
	}

	return nil
}

func downloadTextData(regExp string, html string, outputDir string) int {
	os.Mkdir(outputDir, 0755)

	title := fmt.Sprintf("hop-%d.txt", time.Now().Unix())
	titleRe := regexp.MustCompile(`(?mi)\<\s*?title\s*?\>(.*?)\<\/\s*?title\s*?\>`)
	titleMatches := titleRe.FindStringSubmatch(html)
	if len(titleMatches) > 1 {
		title = fmt.Sprintf("hop-%s-%d.txt", strings.ToLower(titleMatches[1]), time.Now().Unix())
		var re = regexp.MustCompile(`(?mis)[^a-zA-Z0-9\-\_\S]`)
		title = re.ReplaceAllString(title, "")
	}

	var successes int
	var itemsRe = regexp.MustCompile(regExp)

	for _, matches := range itemsRe.FindAllStringSubmatch(html, -1) {
		if len(matches) <= 1 {
			continue
		}

		m := matches[1]
		if m == "" && len(matches) > 2 {
			m = matches[2]
		}

		fName := fmt.Sprintf("%s/%s", outputDir, title)
		err := writeTextToFile(fName, m)
		if err != nil {
			continue
		}

		successes++
	}

	return successes
}

func downloadContent(regExp string, parentURL string, html string, outputDir string) int {
	os.Mkdir(outputDir, 0755)

	var successes int
	var itemsRe = regexp.MustCompile(regExp)

	for _, matches := range itemsRe.FindAllStringSubmatch(html, -1) {
		if len(matches) <= 1 {
			continue
		}

		m := matches[1]
		if m == "" && len(matches) > 2 {
			m = matches[2]
		}

		var re = regexp.MustCompile(`(?mis)^data\:image\/(gif|png|jpe?g)\;base64\,(.*?)$`)
		imgMatches := re.FindStringSubmatch(m)

		var fName string
		var reader io.Reader
		if len(imgMatches) > 2 {
			fName = fmt.Sprintf("%s/%s.%s", outputDir, imgMatches[2], imgMatches[1])
			reader = base64.NewDecoder(base64.StdEncoding, strings.NewReader(imgMatches[2]))

		} else {
			parsedURL, err := url.Parse(m)
			if err != nil {
				continue
			}

			if parsedURL.Hostname() == "" {
				parsedParentURL, err := url.Parse(fmt.Sprintf("%s%s", parentURL, m))
				if err != nil {
					continue
				}
				parsedURL.Host = parsedParentURL.Host
				parsedURL.Scheme = parsedParentURL.Scheme
			}

			m = parsedURL.String()
			println(m)
			resp, err := getURL(m)
			if err != nil {
				continue
			}
			defer resp.Body.Close()
			reader = resp.Body
		}

		fName = fmt.Sprintf("%s/hop-%s", outputDir, filepath.Base(m))
		err := writeContentToFile(fName, reader)
		if err != nil {
			continue
		}

		successes++
	}

	return successes
}
