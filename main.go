package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	. "gitlab.com/ren-codes/hop/api"
)

var (
	c     = flag.String("c", "./.hop", "")
	usage = `Usage: webparser [options...]

	Uses a .webparser JSON config file to parse data out of website

Options:
	-c	JSON config file to use for parsing instead of the .webparser file.
	-h	Shows this menu
`
)

func createConfig() {
	var err error
	defer func() {
		if err != nil {
			println(errors.Wrap(err, "exiting"))
			os.Exit(1)
		}
	}()

	println("configuration setup...")

	type config struct {
		URL        string
		OutputDir  string
		LookupType string
	}

	var m config

	fmt.Print("enter url: ")
	fmt.Scanln(&m.URL)

	fmt.Print("enter output directory (defaults to ./hop): ")
	fmt.Scanln(&m.OutputDir)

	fmt.Print("lookup [images/links/text] (defaults to text): ")
	fmt.Scanln(&m.LookupType)
	mappedConfig, _ := json.Marshal(&m)

	if err = ioutil.WriteFile(*c, mappedConfig, 0644); err != nil {
		return
	}
}

func main() {
	var err error
	defer func() {
		if err != nil {
			println(errors.Wrap(err, "exiting"))
			os.Exit(1)
		}
	}()

	println("start")
	configFile, err := ioutil.ReadFile(*c)
	if err != nil {
		println("config file not found")
		createConfig()
		configFile, err = ioutil.ReadFile(*c)
		if err != nil {
			println("failed to create config file")
			return
		}
	}

	fmt.Printf("parsing config file %s\n", *c)
	var configJSON map[string]string
	if err = json.Unmarshal(configFile, &configJSON); err != nil {
		println("could not parse config file")
		return
	}

	url := configJSON["URL"]
	defer delete(configJSON, "URL")
	fmt.Printf("running hop on url %s\n", url)

	h, err := Hop(url)
	if err != nil {
		fmt.Printf("could not initialize Hop with url %s\n", url)
		return
	}

	for key, val := range configJSON {
		switch strings.ToLower(key) {
		case "cssregex":
			h.CSSRegex = val
			break
		case "lookuptype":
			h.LookupType = val
			break
		case "followlinks":
			h.FollowLinks, err = strconv.ParseBool(val)
			if err != nil {
				fmt.Printf("follow links value %v is not a valid boolean value", val)
				return
			}
			break
		case "outputdir":
			h.OutputDir = val
			break
		case "followdepth":
			h.FollowDepth, err = strconv.Atoi(val)
			if err != nil {
				return
			}
			break
		}
	}

	successCount, err := h.Scrape()
	if err != nil {
		println("there was an error running scrape")
		return
	}

	fmt.Printf("downloaded %d items\n", successCount)
	println("finished")
}
